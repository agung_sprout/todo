<form id="todoForm">
    <div style="margin-bottom: 10px">
        <input type="text" name="todo" id="todoText"/>
        <div>
            <span id="typing">type in a new todo ...</span>
        </div>
    </div>
    <div>
        <button type="submit">Add Todo</button>
    </div>

</form>

<div id="listTodo">
    <?php foreach($model as $m){ ?>
        <div class="list-<?= $m->id?>">
        <span class="todoItem <?= ($m->status === 'DONE' ? 'done' : '')?>" data-id="<?= $m->id?>">
            <?= $m->todoName ?> 
            </span>
            <span class="removeItem" data-id="<?= $m->id?>">X</span>
        </div>
            <?php } ?>
</div>



<button style="margin-top:20px" id="delete-done">delete done task</button>
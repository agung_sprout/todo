<?php
use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?= Html::encode($this->title) ?></title>

    <style>
        label.error{
            color:red;
            font-size: 12px;
            margin-left: 5px;
        }
        .done{
            text-decoration: line-through;
        }
        .removeItem{
            margin-left:5px;display:inline-block; cursor:pointer
        }
    </style>
    <input type="hidden" id="token" value="<?= Yii::$app->request->csrfToken?>"> 
    <input type="hidden" id="base" value="<?= Yii::$app->params['base_url'] ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta property="og:type" content="website" />
  <?php $this->head() ?>
  <meta name="author" content="Agung Septiyadi">
  </head>
  <body>

    <?= $content ?>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/assets/js/vendor/jquery-1.11.3.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/assets/js/vendor/jquery-validate.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/assets/js/main.js"></script>
  </body>
</html>
<?php $this->endPage() ?>

var base = $('#base').val();
var token = $('#token').val();
$('#todoForm').validate({
    rules : {
      todo : {
        required : true
      }
    },
    messages : {
      todo : {
        required : 'please fill the todo'
      }
    },
    submitHandler : function(form){
        var dataform = $(form).serialize() + '&_csrf='+token;
        $.ajax({
            url : base + 'site/create',
            type : 'POST',
            data : dataform,
            success : function(data){
              document.getElementById('todoText').value = '';
              $('#typing').html('type in a new todo ...');
              var newData = data.split('&');
              $('#listTodo').append('<div class="list-'+newData[0]+'"><span data-id="'+newData[0]+'" class="todoItem">'+newData[1]+'</span><span class="removeItem" data-id="'+newData[0]+'">X</span></div>')
            }
          });
        return false;
      }
  });

    $('#todoText').keydown(function(){
        console.log(1)
        setTimeout(function(){
            var todo = $('#todoText').val()
            console.log(todo.length)
            if(todo.length === 0){
                $('#typing').html('type in a new todo ...')
            }else{
                $('#typing').html('typing : ' + todo)
            }
        },100)
       
    })

$(document).on('click','.removeItem',function(){
    var id = $(this).data('id');
    $.ajax({
        url : base + 'site/remove',
        type : 'POST',
        data : {
            id : id,
            _csrf : token
        },
        success : function(data){
          $('.list-'+id).remove();
        }
    });
});
$(document).on('click','.todoItem',function(){
    var id = $(this).data('id');
    $.ajax({
        url : base + 'site/done',
        type : 'POST',
        data : {
            id : id,
            _csrf : token
        },
        success : function(data){
          $('.list-'+id).addClass('done');
        }
    });
});
$(document).on('click','#delete-done',function(){
    var id = $(this).data('id');
    $.ajax({
        url : base + 'site/deletedone',
        type : 'POST',
        data : {
            id : id,
            _csrf : token
        },
        success : function(data){
          $('.done').remove();
        }
    });
});
// function getData(){
//     $.ajax({
//         url : base + 'site/getdata',
//         type : 'GET',
//         success : function(data){
//           console.log(data)
//         }
//       });
// }
// getData();
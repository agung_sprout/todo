<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\Todo;



class SiteController extends Controller
{

    public function actionIndex()
    {
        $model = Todo::find()->where(['in','status',['GOING','DONE']])->all();
        return $this->render('index',[
            'model' => $model
        ]);
    }

    public function actionCreate(){
        $model = new Todo();
        $model->todoName = Yii::$app->request->post('todo');
        $model->save();

        return $model->id.'&'.$model->todoName;
    }
    public function actionRemove(){
        $model = Todo::findOne(Yii::$app->request->post('id'));
        $model->status = 'DELETED';
        $model->save();
    }
    public function actionDone(){
        $model = Todo::findOne(Yii::$app->request->post('id'));
        $model->status = 'DONE';
        $model->save();
    }
    public function actionDeletedone(){
        $model = Todo::updateAll(['status'=>'DELETED'],['=','status','DONE']);
    }
}

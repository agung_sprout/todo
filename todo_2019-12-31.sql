# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.18)
# Database: todo
# Generation Time: 2019-12-31 05:21:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table todo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `todo`;

CREATE TABLE `todo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `todoName` varchar(255) NOT NULL DEFAULT '',
  `status` enum('GOING','DONE','DELETED') DEFAULT 'GOING',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `todo` WRITE;
/*!40000 ALTER TABLE `todo` DISABLE KEYS */;

INSERT INTO `todo` (`id`, `todoName`, `status`, `createdAt`)
VALUES
	(3,'cus','DELETED','2019-12-31 12:21:07'),
	(4,'asasd','DELETED','2019-12-31 12:12:34'),
	(5,'am','DELETED','2019-12-31 12:01:11'),
	(6,'asdasda','DELETED','2019-12-31 12:12:35'),
	(7,'asdad','DELETED','2019-12-31 12:12:36'),
	(8,'asdasdasd','DELETED','2019-12-31 12:12:01'),
	(9,'asdasda','DELETED','2019-12-31 12:12:02'),
	(10,'asdasd','DELETED','2019-12-31 12:12:02'),
	(11,'asdasdasd','DELETED','2019-12-31 12:12:03'),
	(12,'adsasdas','DELETED','2019-12-31 12:12:04'),
	(13,'asdasd','DELETED','2019-12-31 12:12:36'),
	(14,'abj','DELETED','2019-12-31 12:12:37'),
	(15,'asdasdsa','DELETED','2019-12-31 12:12:37'),
	(16,'asdasdas','DELETED','2019-12-31 12:13:49'),
	(17,'asdasdasd','DELETED','2019-12-31 12:13:48'),
	(18,'kuy','DELETED','2019-12-31 12:13:51'),
	(19,'ah','DELETED','2019-12-31 12:20:08'),
	(20,'yus','DELETED','2019-12-31 12:20:08'),
	(21,'asdasdas','DELETED','2019-12-31 12:17:27'),
	(22,'asdasd','DELETED','2019-12-31 12:14:21'),
	(23,'asdasdjhb','DELETED','2019-12-31 12:17:42'),
	(24,'asdasd','DELETED','2019-12-31 12:20:51'),
	(25,'us','DELETED','2019-12-31 12:20:40'),
	(26,'guys','DELETED','2019-12-31 12:20:34'),
	(27,'yap','DELETED','2019-12-31 12:21:07'),
	(28,'upda','GOING','2019-12-31 12:20:54'),
	(29,'asdasd','DELETED','2019-12-31 12:21:01'),
	(30,'asd','DELETED','2019-12-31 12:21:01'),
	(31,'as','GOING','2019-12-31 12:20:56'),
	(32,'as','DELETED','2019-12-31 12:21:01'),
	(33,'as','DELETED','2019-12-31 12:21:01'),
	(34,'as','GOING','2019-12-31 12:20:56');

/*!40000 ALTER TABLE `todo` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
